﻿using common.libs;
using common.libs.extends;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace common.tcpforward
{
    public class TcpForwardResolver
    {
        private readonly TcpForwardMessengerSender tcpForwardMessengerSender;
        private readonly Config config;
        private ConcurrentDictionary<ConnectionKey, ConnectUserToken> connections = new ConcurrentDictionary<ConnectionKey, ConnectUserToken>(new ConnectionComparer());

        public TcpForwardResolver(TcpForwardMessengerSender tcpForwardMessengerSender, Config config)
        {
            this.tcpForwardMessengerSender = tcpForwardMessengerSender;
            this.config = config;

            //B接收到A的请求
            tcpForwardMessengerSender.OnRequestHandler.Sub(OnRequest);
        }

        private void OnRequest(SendArg arg)
        {
            ConnectionKey key = new ConnectionKey(arg.Connection.ConnectId, arg.Data.RequestId);
            if (connections.TryGetValue(key, out ConnectUserToken token))
            {
                if (arg.Data.Buffer.Length > 0)
                {
                    token.TargetSocket.Send(arg.Data.Buffer.Span);
                }
                else
                {
                    token.Clear();
                    connections.TryRemove(token.Key, out _);
                }
            }
            else
            {
                Connect(arg);
            }
        }
        private void Connect(SendArg arg)
        {
            IPEndPoint endpoint = NetworkHelper.EndpointFromArray(arg.Data.TargetEndpoint);
            if (!config.LanConnectEnable && arg.Data.ForwardType == TcpForwardTypes.PROXY && NetworkHelper.IsLan(endpoint))
            {
                Receive(arg, Helper.EmptyArray);
                return;
            }
            if (!Intercept(arg, endpoint.Port))
            {
                Receive(arg, Helper.EmptyArray);
                return;
            }
            Socket socket = new(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);

            SocketAsyncEventArgs saea = new SocketAsyncEventArgs();

            saea.RemoteEndPoint = endpoint;
            saea.Completed += IO_Completed;
            if (arg.Data.Buffer.Length > 0 && arg.Data.ForwardType != TcpForwardTypes.PROXY)
            {
                saea.SetBuffer(arg.Data.Buffer);
            }
            saea.UserToken = new ConnectUserToken
            {
                Key = new ConnectionKey(arg.Connection.ConnectId, arg.Data.RequestId),
                SendArg = new SendArg
                {
                    Connection = arg.Connection,
                    Data = new TcpForwardInfo
                    {
                        RequestId = arg.Data.RequestId,
                        TargetEndpoint = arg.Data.TargetEndpoint,
                        AliveType = arg.Data.AliveType,
                        ForwardType = arg.Data.ForwardType,
                    }
                }
            };

            if (!socket.ConnectAsync(saea))
            {
                ProcessConnect(saea);
            }
        }

        private void IO_Completed(object sender, SocketAsyncEventArgs e)
        {
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Connect:
                    ProcessConnect(e);
                    break;
                case SocketAsyncOperation.Receive:
                    ProcessReceive(e);
                    break;
                default:
                    break;
            }
        }

        private void ProcessConnect(SocketAsyncEventArgs e)
        {
            ConnectUserToken token = (ConnectUserToken)e.UserToken;
            try
            {
                if (e.SocketError == SocketError.Success)
                {
                    if (token.SendArg.Data.ForwardType == TcpForwardTypes.PROXY)
                    {
                        Receive(token, HttpConnectMethodHelper.ConnectSuccessMessage());
                    }

                    token.TargetSocket = e.ConnectSocket;
                    e.SetBuffer(new byte[config.BufferSize], 0, config.BufferSize);
                    connections.TryAdd(token.Key, token);
                    if (!token.TargetSocket.ReceiveAsync(e))
                    {
                        ProcessReceive(e);
                    }
                }
                else
                {
                    if (token.SendArg.Data.ForwardType == TcpForwardTypes.PROXY)
                    {
                        Receive(token, HttpConnectMethodHelper.ConnectErrorMessage());
                    }
                    CloseClientSocket(token);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.DebugError(ex);
                CloseClientSocket(token);
            }
        }

        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            ConnectUserToken token = (ConnectUserToken)e.UserToken;

            try
            {
                if (e.BytesTransferred > 0 && e.SocketError == SocketError.Success)
                {
                    Receive(token, e.Buffer.AsMemory(0, e.BytesTransferred));

                    if (token.TargetSocket.Available > 0)
                    {
                        var arr = ArrayPool<byte>.Shared.Rent(token.TargetSocket.Available);
                        while (token.TargetSocket.Available > 0)
                        {
                            int length = token.TargetSocket.Receive(arr);
                            if (length > 0)
                            {
                                Receive(token, arr.AsMemory(0, length));
                            }
                        }

                        ArrayPool<byte>.Shared.Return(arr);
                    }

                    if (!token.TargetSocket.ReceiveAsync(e))
                    {
                        ProcessReceive(e);
                    }
                }
                else
                {
                    CloseClientSocket(token);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.DebugError(ex);
                CloseClientSocket(token);
            }
        }

        private void CloseClientSocket(ConnectUserToken token)
        {
            Receive(token, Helper.EmptyArray);
            token.Clear();
            connections.TryRemove(token.Key, out _);
        }

        private bool Intercept(SendArg arg, int port)
        {
            if (!config.Enable)
            {
                return false;
            }
            if (config.PortWhiteList.Length > 0 && !config.PortWhiteList.Contains(port))
            {
                return false;
            }
            if (config.PortBlackList.Contains(port))
            {
                return false;
            }
            return true;
        }

        private void Receive(ConnectUserToken token, in Memory<byte> data)
        {
            Receive(token.SendArg, data);
        }
        private void Receive(SendArg arg, in Memory<byte> data)
        {
            arg.Data.Buffer = data;
            tcpForwardMessengerSender.SendResponse(arg).ConfigureAwait(false).GetAwaiter().GetResult();
        }

    }

    public class ConnectUserToken
    {
        public Socket TargetSocket { get; set; }
        public ConnectionKey Key { get; set; }

        public SendArg SendArg { get; set; }

        public void Clear()
        {
            TargetSocket?.SafeClose();
            TargetSocket = null;
            //SendArg = null;

            GC.Collect();
            GC.SuppressFinalize(this);
        }
    }

    public class ConnectionComparer : IEqualityComparer<ConnectionKey>
    {
        public bool Equals(ConnectionKey x, ConnectionKey y)
        {
            return x.RequestId == y.RequestId && x.ConnectId == y.ConnectId;
        }

        public int GetHashCode(ConnectionKey obj)
        {
            return 0;
        }
    }
    public readonly struct ConnectionKey
    {
        public readonly ulong RequestId { get; }
        public readonly ulong ConnectId { get; }

        public ConnectionKey(ulong connectId, ulong requestId)
        {
            ConnectId = connectId;
            RequestId = requestId;
        }
    }
}