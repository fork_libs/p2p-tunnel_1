﻿using System;

namespace common.socks5
{
    public class Socks5ClientHandler : ISocks5ClientHandler
    {
        public Socks5ClientHandler()
        {
        }

        public Socks5EnumAuthType HandleRequest(ulong id, Memory<byte> buffer)
        {
            return Socks5EnumAuthType.NoAuth;
        }

        public Socks5EnumAuthState HandleAuth(ulong id, Memory<byte> buffer)
        {
            return Socks5EnumAuthState.UnKnow;
        }

        public Socks5EnumResponseCommand HandleCommand(ulong id, Memory<byte> buffer)
        {
           return Socks5EnumResponseCommand.ServerError;
        }

        public void HndleForward(ulong id, Memory<byte> buffer)
        {
        }

        public void Close(ulong id)
        {
        }

        public void Flush()
        {
            throw new NotImplementedException();
        }
    }
}
