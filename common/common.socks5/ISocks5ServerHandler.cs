﻿using common.server;

namespace common.socks5
{
    public interface ISocks5ServerHandler
    {
        Socks5EnumAuthType HandleRequest(Socks5Info data);
        Socks5EnumAuthState HandleAuth(Socks5Info data);
        void HndleForward(IConnection connection, Socks5Info data);
        Socks5EnumResponseCommand HandleCommand(IConnection connection, Socks5Info data);
    }
}
